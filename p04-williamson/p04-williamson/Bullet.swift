//
//  Bullet.swift
//  p04-williamson
//
//  Created by Matthew Williamson on 2/16/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

import SpriteKit

class Bullet : SKNode {
    
    var node: SKSpriteNode
    
    init(ship: Ship) {
        node = SKSpriteNode(imageNamed: "Bullets")
        node.setScale(1.0)
        
        super.init()
        name = "bullet"
        physicsBody = SKPhysicsBody(circleOfRadius: CGFloat(node.size.width/2))
        physicsBody?.categoryBitMask = bulletCategory
        physicsBody?.contactTestBitMask = asteroidCategory
        physicsBody?.collisionBitMask = 0
        
        position = ship.position
        let angle = ship.zRotation
        physicsBody?.velocity = CGVectorMake(ship.physicsBody!.velocity.dx + sin(angle) * -120, ship.physicsBody!.velocity.dy + cos(angle) * 120)
        physicsBody?.linearDamping = 0

        addChild(node)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}