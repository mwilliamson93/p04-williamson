//
//  GameScene.swift
//  p04-williamson
//
//  Created by Matthew Williamson on 2/12/16.
//  Copyright (c) 2016 Matthew Williamson. All rights reserved.
//

import SpriteKit

let shipCategory: UInt32 =  0x1 << 0;
let bulletCategory: UInt32 =  0x1 << 1;
let asteroidCategory: UInt32 =  0x1 << 2;

class GameScene: SKScene, SKPhysicsContactDelegate {

    var ship: Ship?
    var score: Int = 0
    
    // Setup your scene here
    override func didMoveToView(view: SKView) {
        let bgImage = SKSpriteNode(imageNamed: "galaxy-2048x1152.jpg")
        bgImage.position = CGPointMake(size.width/2, size.height/2)
        bgImage.zPosition = 0
        addChild(bgImage)
        
        physicsWorld.gravity = CGVectorMake(0,0);
        physicsWorld.contactDelegate = self;

        ship = Ship(pos: CGPoint(x:size.width/2, y:size.height/2))
        ship?.zPosition = 1
        addChild(ship!)
        makeAsteroids()
        
        let leftButton = UIButton()
        makeButton(leftButton, x: 15, y: size.height-120, title: "<", act:"turnLeft")
        let rightButton = UIButton()
        makeButton(rightButton, x: 80, y: size.height-60, title: ">", act:"turnRight")
        let shootButton = UIButton()
        makeButton(shootButton, x: size.width-55, y: size.height-120, title: "S", act:"shoot")
        let engineButton = UIButton()
        makeButton(engineButton, x: size.width-120, y: size.height-60, title: "E", act:"engine")
    }
    
    func makeButton(button: UIButton, x: CGFloat, y: CGFloat, title: String, act: Selector) {
        let size: CGFloat = 45
        let borderAlpha: CGFloat = 1.0
        button.frame = CGRectMake(x, y, size, size)
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.backgroundColor = UIColor.clearColor()
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor(white: 1.0, alpha: borderAlpha).CGColor
        button.addTarget(self, action: act, forControlEvents: .TouchUpInside)
        button.layer.cornerRadius = size/2
        self.view!.addSubview(button)
    }
    
    func turnLeft() {
        ship!.zRotation += 0.5
    }
    
    func turnRight() {
        ship!.zRotation -= 0.5
    }
    
    func shoot() {
        let bullet = Bullet(ship: ship!)
        bullet.zPosition = 1
        addChild(bullet)
    }
    
    func engine() {
        let angle = ship!.zRotation
        ship?.physicsBody?.applyImpulse(CGVectorMake(sin(angle) * -2, cos(angle) * 2))
    }

    func makeAsteroids() {
        for _ in 1...3 {
           let asteroid = Asteroid(pos: CGPoint(x: size.width - CGFloat(arc4random_uniform(UInt32(size.width * 0.5))), y: CGFloat(arc4random_uniform(UInt32(size.height)))))
            asteroid.zPosition = 1
            addChild(asteroid)
        }
    }
    
    // Called when items in the frame contact
    func didBeginContact(contact: SKPhysicsContact) {
        var a: SKPhysicsBody!
        var b: SKPhysicsBody!

        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            a = contact.bodyA
            b = contact.bodyB
        }
        else {
            a = contact.bodyB
            b = contact.bodyA
        }
            
        if (a.categoryBitMask & bulletCategory) != 0 && (b.categoryBitMask & asteroidCategory) != 0 {
            if (a.node != nil) {
                asteroidHit(b.node as! Asteroid)
                (a.node as! Bullet).removeFromParent()
                score += 10
            }
        }
            
        if (a.categoryBitMask & shipCategory) != 0 && (b.categoryBitMask & asteroidCategory) != 0 {
            asteroidHit(b.node as! Asteroid)
            (a.node as! Ship).removeFromParent()
            let reveal = SKTransition.crossFadeWithDuration(0.5)
            let gameoverScene = GameOverScene(size: size, score: score)
            self.view?.presentScene(gameoverScene, transition: reveal)
        }
    }
    
    // Called before each frame is rendered
    override func update(currentTime: CFTimeInterval) {
        ship!.update(currentTime)
        removeBullets()
        
        var asteroidsRemaining = false
        enumerateChildNodesWithName("asteroid", usingBlock:  {
            (node: SKNode, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
            (node as! Asteroid).update(currentTime)
            asteroidsRemaining = true
        })
        
        if (!asteroidsRemaining) {
            makeAsteroids()
        }
        
    }
    
    func asteroidHit(a: Asteroid) {
        a.removeFromParent()
        let newSize = a.nodeSize - 1
        if (newSize != 0) {
            for _ in 1...2 {
                let newAsteroid = Asteroid(s: newSize, pos: a.position)
                newAsteroid.zPosition = 1
                addChild(newAsteroid)
             }
        }
    }
    
    func removeBullets() {
        enumerateChildNodesWithName("bullet", usingBlock:  {
            (node: SKNode, stop: UnsafeMutablePointer <ObjCBool>) -> Void in
            let x = node.position.x
            let y = node.position.y
            if (x < 0 || x > self.size.width || y < 0 || y > self.size.height) {
                node.removeFromParent()
            }
        })
    }
   
    
}
