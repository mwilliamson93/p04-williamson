//
//  GameoverScene.swift
//  p04-williamson
//
//  Created by Matthew Williamson on 2/17/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    
    init(size: CGSize, score: Int) {
        super.init(size: size)
        backgroundColor = SKColor(colorLiteralRed: 0.03, green: 0.22, blue: 0.39, alpha: 1)
        
        let label = SKLabelNode(fontNamed: "Futura-MediumItalic")
        label.text = "Score: " + String(score)
        label.fontSize = 50
        label.fontColor = SKColor.whiteColor()
        label.position = CGPoint(x: size.width/2, y: size.height/2)
        addChild(label)
        
        runAction(SKAction.sequence([
            SKAction.waitForDuration(2.0),
            SKAction.runBlock() {
                let reveal = SKTransition.crossFadeWithDuration(1.0)
                let scene = GameScene(size: size)
                self.view?.presentScene(scene, transition:reveal)
            }
            ])
        )
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}